read -p "AccessKey: " Accesskey
read -p "SecretKey: " Secretkey
read -p "Region: " region
read -p "PrivateKeyFileName: " privatekey

if [[ ! -f $privatekey ]]
then
    echo "Private Key File with name ${privatekey} is not exist"
    exit 0
fi

if [[ -z $Accesskey || -z $Secretkey ]]
then
    echo "please fill out Access key or secretkey"
    exit 0
fi

if [[ -z $Region ]]
then
    echo "Using default region to ap-southeast-1"
    region="ap-southeast-1"
fi

pubkey=`ssh-keygen -y -f ${privatekey}`
sed -i "s/YOURACCESSKEY/$Accesskey/g" Terraform.tfvars
sed -i "s/YOURSECRETKEY/$Secretkey/g" Terraform.tfvars
sed -i "s/YOURREGION/$region/g" Terraform.tfvars
sed -i "s/THEPUBKEY/$pubkey/g" Terraform.tfvars
MyIP=`curl -s ifconfig.co`
sed -i "s/MYIP/$MyIP\/32/g" vars.tf

#initiating terraform
terraform init # > /dev/null

#starting provision instances
terraform apply --auto-approve  # > /dev/null

AlphaServerPubIP=`terraform output AlphaServerEIP`
AlphaServerPrivIP=`terraform output AlphaServerPrivateIP`
AlphaClientPubIP=`terraform output AlphaClientEIP`

sed -i "s/$AlphaServerPubIP/AlphaServerIP/g" hosts/alpha
sed -i "s/$AlphaServerPrivIP/AlphaServerPrivateIP/g" template/22-remote.conf
sed -i "s/$AlphaClientPubIP/AlphaClientIP/g" Terraform.tfvars

#start ansible config
ansible-playbook playbook.yml -i  hosts/*
sed -i "s/$Accesskey/YOURACCESSKEY/g" Terraform.tfvars
sed -i "s/$Secretkey/YOURSECRETKEY/g" Terraform.tfvars
sed -i "s/$region/YOURREGION/g" Terraform.tfvars
sed -i "s/$pubkey/THEPUBKEY/g" Terraform.tfvars
sed -i "s/$MyIP\/32/MYIP/g" vars.tf
sed -i "s/AlphaServerIP/$AlphaServerPubIP/g" hosts/alpha
sed -i "s/AlphaServerPrivateIP/$AlphaServerPrivIP/g" template/22-remote.conf
sed -i "s/AlphaClientIP/$AlphaClientPubIP/g" Terraform.tfvars