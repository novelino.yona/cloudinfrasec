output "AlphaServerEIP" {
  value = aws_eip.AlphaServerEIP.public_ip
}

output "AlphaServerPrivateIP" {
  value = aws_instance.AlphaServer.private_ip
}

output "AlphaClientEIP" {
  value = aws_eip.AlphaClientEIP.public_ip
}