#Create keypair
resource "aws_key_pair" "Alpha-kp" {
  key_name   = "Alpha-kp"
  public_key = var.pub_key
}

#Alpha Server Resources
resource "aws_network_interface" "AlphaServerInt" {
  subnet_id   = aws_subnet.AlphaServerSubnet.id
  security_groups = [aws_security_group.AlphaServer-sg.id]
  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_eip" "AlphaServerEIP" {
  network_interface = aws_network_interface.AlphaServerInt.id
  vpc      = true
    tags = {
    Name = "AlphaServerEIP"
  }
}

resource "aws_instance" "AlphaServer" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.medium"
  key_name      = aws_key_pair.Alpha-kp.id
  network_interface {
    network_interface_id = aws_network_interface.AlphaServerInt.id
    device_index         = 0
  }
  tags = {
    Name = "AlphaServer"
  }
  user_data = <<EOF
  #!/bin/bash
  wget http://nginx.org/keys/nginx_signing.key
  apt-key add nginx_signing.key
  cd /etc/apt
  echo "deb http://nginx.org/packages/ubuntu focal nginx" >> sources.list
  echo "deb-src http://nginx.org/packages/ubuntu focal nginx" >> sources.list
  apt-get update
  apt-get -y install nginx
  systemctl start nginx.service
  systemctl enable nginx.service
  EOF
}

#Alpha Client Resources
resource "aws_network_interface" "AlphaClientInt" {
  subnet_id   = aws_subnet.AlphaClientSubnet.id
  security_groups = [aws_security_group.AlphaClient-sg.id]
  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_eip" "AlphaClientEIP" {
  network_interface = aws_network_interface.AlphaClientInt.id
  vpc      = true
    tags = {
    Name = "AlphaClientEIP"
  }
}

resource "aws_instance" "AlphaClient" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.medium"
  key_name      = aws_key_pair.Alpha-kp.id
  network_interface {
    network_interface_id = aws_network_interface.AlphaClientInt.id
    device_index         = 0
  }
  tags = {
    Name = "AlphaClient"
  }
}