#Create security groups
resource "aws_security_group" "AlphaServer-sg" {
  name   = "AlphaServer-sg" 
  vpc_id = aws_vpc.Main.id
  dynamic "ingress" {
    for_each = var.AlphaServer_Default_ingress_rules
    content {
      from_port   = ingress.value.from
      to_port     = ingress.value.to
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr
      description = ingress.value.description
    }
  }

  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      description = "Allow all traffic to internet"
    }
}

resource "aws_security_group" "AlphaClient-sg" {
  name   = "AlphaClient-sg" 
  vpc_id = aws_vpc.Main.id
  dynamic "ingress" {
    for_each = var.AlphaClient_Default_ingress_rules
    content {
      from_port   = ingress.value.from
      to_port     = ingress.value.to
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr
      description = ingress.value.description
    }
  }

  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      description = "Allow all traffic to internet"
    }
}


#add additional rule to accept syslog connection from AlphaClient to AlphaServer
resource "aws_security_group_rule" "AdditionalAlphaServer-sg"  {
  type                     = "ingress"
  from_port                = 514
  to_port                  = 514
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.AlphaClient-sg.id
  security_group_id        = aws_security_group.AlphaServer-sg.id
}
