variable "region" {}
variable "secret_key" {}
variable "access_key" {}
variable "main_vpc_cidr" {}
variable "AlphaServerSubnet" {}
variable "AlphaClientSubnet" {}
variable "instance_type" {}
variable "pub_key" {}
variable "AlphaServer_Default_ingress_rules" {
  type = list(object({
    from        = number
    to          = number
    protocol    = string
    cidr        = list(string)
    description = string
  }))
  default = [ {
    cidr = ["MYIP"]
    from = 22
    protocol = "tcp"
    to = 22
    description = "Allow SSH from Users IP"
},
{ cidr = ["MYIP"]
  from = 80
  protocol = "tcp"
  to = 80
  description = "Allow HTTP Access from everywhere"
  }
   ]
}
variable "AlphaClient_Default_ingress_rules" {
  type = list(object({
    from        = number
    to          = number
    protocol    = string
    cidr        = list(string)
    description = string
  }))
  default = [ {
    cidr = ["0.0.0.0/0"]
    from = 22
    protocol = "tcp"
    to = 22
    description = "Allow SSH from Anywhere"
}]
}