#Create the VPC
resource "aws_vpc" "Main" {            # Creating VPC here
  cidr_block       = var.main_vpc_cidr # Defining the CIDR block use 10.0.0.0/24 for demo
  instance_tenancy = "default"
  tags= {
    Name = "Alpha VPC"
  }
}
#Create Internet Gateway and attach it to VPC
resource "aws_internet_gateway" "IGW" { # Creating Internet Gateway
  vpc_id = aws_vpc.Main.id              # vpc_id will be generated after we create VPC
}

#Route table for Alpha Server Subnet's
resource "aws_route_table" "PublicRT" { # Creating RT for Alpha Server Subnet
  vpc_id = aws_vpc.Main.id
  route {
    cidr_block = "0.0.0.0/0" # Traffic from Alpha Server Subnet reaches Internet via Internet Gateway
    gateway_id = aws_internet_gateway.IGW.id
  }
}

#Create a Alpha Server Subnets.
resource "aws_subnet" "AlphaServerSubnet" { # Creating Alpha Server Subnets
  vpc_id     = aws_vpc.Main.id
  cidr_block = var.AlphaServerSubnet # CIDR block of Alpha Server Subnets
  tags = {
    Name = "AlphaServerSubnet"
  }
}
#Create a Alpha Client Subnet                   # Creating Alpha Client Subnets
resource "aws_subnet" "AlphaClientSubnet" {
  vpc_id     = aws_vpc.Main.id
  cidr_block = var.AlphaClientSubnet # CIDR block of Alpha Client subnets
  tags = {
    Name = "AlphaClientSubnet"
  }
}

#Route table Association with Alpha Server Subnet's
resource "aws_route_table_association" "AlphaServerSubnetRTassociation" {
  subnet_id      = aws_subnet.AlphaServerSubnet.id
  route_table_id = aws_route_table.PublicRT.id
}
#Route table Association with Alpha Client Subnet's
resource "aws_route_table_association" "AlphaClientSubnetRTassociation" {
  subnet_id      = aws_subnet.AlphaClientSubnet.id
  route_table_id = aws_route_table.PublicRT.id
}


