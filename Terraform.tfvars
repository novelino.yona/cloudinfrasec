main_vpc_cidr = "172.16.0.0/24"
AlphaServerSubnet = "172.16.0.0/25"
AlphaClientSubnet = "172.16.0.128/25"
region = "YOURREGION" #Please change this accordingly to destinated region
access_key = "YOURACCESSKEY"
secret_key = "YOURSECRETKEY" 
instance_type = "t2.medium"
pub_key = "THEPUBKEY"
